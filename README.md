# Von-Neumann vs Hardvard

```mermaid
graph TD;
    
    B-->F(Modelos);
    subgraph " "
    F-->G(Harvard);
    G--Origen-->N(Creado por Howard H. Aiken en 1945);
    G--Configuracion-->I(Datos e instrucciones de un programa que se <br>encuentran en celdas separadas de memoria);
    G--Carecteristicas-->J(Posee dos tipos de memorias);
    J-->K(Distintas áreas de direcciones de datos y de instrucciones);
    G--Contiene-->L(Microcontroladores y DPS);
    G--Estructura-->M(Procesador <br> Buses <br> Memoria datos <br> Memoria instrucciones);

    F-->H(Von-Neumann);
    H--es-->O(La arquitectura de las computadoras actualmente <br> Desarrollado por John Von-Neumann);
    H-->P(Estructura);
    P-->Q(Almacenamiento);
    P-->R(Perifericos);
    P-->S(Procesador);

    S--Clasificacion-->T(Unidad de control <br> Unidad Aritmetica Logica);
    R--Son-->U(Entrada <br> Salida);
    Q-->V(Memoria RAM <br> Disco Duro <br> ROM);

    
    end

    A{{Von-Neumann vs Hardvard}}--Son-->B(Arquitectura de computadoras);
    B--Historia-->C(John Von-Neumann);
    subgraph " "
    C-->D(Nacio el 28 de diciembre de 1903 en Budapest/Hungria <br> Murio el 8 de Febrero de 1957 en Washington/USA);
    D--Aportaciones-->E(Programa de almacenamiento en memoria de los datos<br>Computadora ENIAC<br>Crea Arquitectura de la Computadora<br>Teoria de Conjuntos<br>Teoria de Juegos y Equilibrio General);
    end

    style A stroke:#14E41A,stroke-width:3px
    style B stroke:#64F768,stroke-width:3px
    style C stroke:#9AFFD7,stroke-width:3px
    style F stroke:#9AFFD7,stroke-width:3px

```


# Supercomputadoras

```mermaid
graph TD;
 A{{Supercomputadoras}}--Definicion-->B(Sistemas de alto rendimiento más rápidos disponibles en un momento dado.);
    B-->C(En Mexico);
    subgraph " "
    C-->D(IBM-650);
    C-->E(CRAY-402);
    C-->F(KamBalam);
    C-->G(Miztli);
    C-->H(Xiuhcoatl);
    C-->I(BUAP);

    D--Origen-->J(Primera gran computadora en LATAM por la UNAM en 1958);
    D--Caracteristicas-->K(Lecura de informacion de tarjetas perforadas<br>Memoria de 1Kb<br>Proceso de 1000 operaciones aritmeticas/s);

    E--Origen-->L(UNAM en 1991, Se consolida en las grandes ligas de la computacion al adquirirla);
    E--Caracteristicas-->M(Primera supercomputadora en America Latina<br>Equivale a 2000 computadoras de oficina<br>En servicio por 10 años por la Ciencia Mexicana);
    
    F--Caracteristicas-->N(Capacidad equivalente a mas de 1300 computadoras de escritorio<br>Uso para proyectos de investigacion como Astronomia, quimica cuantica, nanotecnologia, etc.<br>Procesa de 7 billones de operaciones matematicas/s);

    G--Origen-->O(Inicio sus servicios en 2012 y se encuentra en la UNAM);
    G--Caracteristicas-->P(Capacidad de mas de 8344 procesadores <br> Uso principal para el estudio del universo, farmacos, etc.<br>Realiza al rededor de 120 proyectos de investigacion anualmente);

    H--Origen-->Q(Creado en 2012 en la UNAM);
    H--Caracteristicas-->R(Segunda computadora mas rapida de LATAM<br>Vida estimada de 4 años<br>Uso para la investigacion cientifica);

    I--Caracteristicas-->S(Se considera top 5 de las supercomputadoras mas poderosas de latinoamerica<br>En un segundo ejecuta 2 billones de operaciones<br>Almacenamiento equivalente a mas de 5000 computadoras portatiles<br>Realiza simulaciones<br>Alerta para temperaturas altas<br>Uso en el sector automotriz);
    end
style A stroke:#14E41A,stroke-width:3px
    style B stroke:#64F768,stroke-width:3px
    style C stroke:#3AFC0F,stroke-width:3px
    style F stroke:#9AFFD7,stroke-width:3px
    style H stroke:#9AFFD7,stroke-width:3px
    style I stroke:#9AFFD7,stroke-width:3px
    style G stroke:#9AFFD7,stroke-width:3px
    style E stroke:#9AFFD7,stroke-width:3px
    style D stroke:#9AFFD7,stroke-width:3px
```



